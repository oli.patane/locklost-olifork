import os
import sys
import argparse

from . import set_signal_handlers
from . import config_logger
from . import config
from . import search
from . import analyze
from . import condor


##################################################


def start_job():
    try:
        os.makedirs(config.CONDOR_ONLINE_DIR)
    except OSError:
        pass
    sub = condor.CondorSubmit(
        config.CONDOR_ONLINE_DIR,
        'online',
        local=True,
        restart=True,
        notify_user=os.getenv('CONDOR_NOTIFY_USER'),
    )
    sub.write()
    sub.submit()
    print("""
Run the following to monitor:
watch condor_q -nobatch
tail -F {0}/{{log,out}}
""".format(config.CONDOR_ONLINE_DIR).strip())


def _parser_add_arguments(parser):
    parser = parser.add_subparsers(
        title='Commands',
        metavar='<command>',
        dest='cmd',
    )
    parser.required = True
    sp = parser.add_parser(
        'exec',
        help="directly execute online search",
    )
    sp.add_argument(
        '--analyze', action='store_true',
        help="execute the condor event analysis callback when events are found",
    )
    parser.add_parser(
        'start',
        help="start condor online search",
    )
    parser.add_parser(
        'stop',
        help="stop condor online search",
    )
    parser.add_parser(
        'restart',
        help="restart condor online search",
    )
    parser.add_parser(
        'status',
        help="print online condor job status",
    )
    return parser


def main(args=None):
    """Online search for lockloss events

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    if args.cmd == 'exec':
        if args.analyze:
            event_callback = analyze.analyze_condor
            stat_file = config.ONLINE_STAT_FILE
            if not os.path.exists(stat_file):
                open(stat_file, 'w').close()
        else:
            event_callback = None
            stat_file = None
        search.search_iterate(
            event_callback=event_callback,
            stat_file=stat_file,
        )

    elif args.cmd in ['start', 'stop', 'restart']:
        print("The LIGO site deployments of the online search are now handled via systemd --user on the 'detechar.ligo-?a.caltech.edu' hosts.", file=sys.stderr)
        print("See \"systemctl --user status\" for more info.", file=sys.stderr)
        if input(f"Type 'yes' if you're really sure you want to execute the online condor '{args.cmd}' command: ") != 'yes':
            print("aborting.", file=sys.stderr)
            exit(1)
        ojobs, ajobs = condor.find_jobs()
        if args.cmd == 'start':
            assert not ojobs, "There are already currently acitve jobs:\n{}".format(
                '\n'.join([condor.job_str(job) for job in ojobs]))
        else:
            condor.stop_jobs(ojobs)
        if args.cmd in ['start', 'restart']:
            start_job()

    elif args.cmd == 'status':
        ojobs, ajobs = condor.find_jobs()
        for job in ojobs + ajobs:
            print(condor.job_str(job))


# direct execution of this module intended for condor jobs
if __name__ == '__main__':
    set_signal_handlers()
    config_logger(level='DEBUG')
    stat_file = config.ONLINE_STAT_FILE
    if not os.path.exists(stat_file):
        open(stat_file, 'w').close()
    search.search_iterate(
        event_callback=analyze.analyze_condor,
        stat_file=stat_file,
    )
