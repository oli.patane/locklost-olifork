import numpy as np

from gwpy.segments import Segment

from .. import logger
from .. import config
from .. import data
from .. import plotutils


##############################################


def check_omc_dcpd(event):
    """
    Check if OMC DCPD is saturating.

    Checks OMC_PD overflow channels for values greater than zero, one second
    before the refined lockloss time. If so, creates OMC_DCPD tag.
    """
    plotutils.set_rcparams()

    mod_window = [config.OMC_DCPD_WINDOW[0], config.OMC_DCPD_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    channels = data.fetch(config.OMC_DCPD_CHANNELS, segment)

    saturating = False
    srate = channels[0].sample_rate
    for buf in channels:
        t = np.arange(segment[0], segment[1], 1/srate)
        before_loss = buf.data[np.where(t < event.gps-(config.OMC_DCPD_BUFFER))]
        if any(before_loss) > 0:
            saturating = True

    if saturating:
        event.add_tag('OMC_DCPD')
    else:
        logger.info('OMC DCPD not saturating')
